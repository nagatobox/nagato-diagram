
from libnagato.Object import NagatoObject
from libnagatodiagram.ui.PixbufFactory import NagatoPixbufFactory
from libnagatodiagram.dataovermind.ExportLayer import NagatoExportLayer


class NagatoDotLayer(NagatoObject):

    def _yuki_n_new_file(self, path=None):
        self._factory.create_new_pixbuf(path)
        self._raise("YUKI.N > new file", path)

    def __init__(self, parent):
        self._parent = parent
        self._factory = NagatoPixbufFactory(self)
        NagatoExportLayer(self)
