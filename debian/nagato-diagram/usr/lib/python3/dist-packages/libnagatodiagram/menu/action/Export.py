
from libnagato.menu.Action import NagatoActionCore


class NagatoExport(NagatoActionCore):

    def _initialize_variables(self):
        self._title = "Export"
        self._query = "YUKI.N > has image"
        self._message = "YUKI.N > export graph"
