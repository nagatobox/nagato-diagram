
from libnagato.Object import NagatoObject
from libnagato.dialog.chooser.Save import NagatoSave


class NagatoPixbufExport(NagatoObject):

    def _call_dialog(self, pixbuf):
        yuki_path = NagatoSave.call(current_name="untitled.png")
        if yuki_path is not None:
            pixbuf.savev(yuki_path, "png", [], [])

    def export(self):
        yuki_pixbuf = self._enquiry("YUKI.N > pixbuf")
        if yuki_pixbuf is not None:
            self._call_dialog(yuki_pixbuf)

    def __init__(self, parent):
        self._parent = parent
