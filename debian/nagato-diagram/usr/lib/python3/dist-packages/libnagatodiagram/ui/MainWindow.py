
from libnagato.ui.MainWindow import NagatoMainWindow as TFEI
from libnagatodiagram.dataovermind.PortalLayer import NagatoPortalLayer
from libnagato.Mikuru import HomeDirectory


class NagatoMainWindow(TFEI):

    def _on_close_window(self, widget, event, user_data=None):
        if not self._closure.get_closable():
            return True
        return self._gtk_main_quit()

    def _yuki_n_register_closure_object(self, object_):
        self._closure = object_

    def _yuki_n_add_to_main_window(self, widget):
        self.add(widget)

    def _yuki_n_new_file(self, file_name=None):
        if file_name is None:
            self.set_title("nagato-diagram")
        else:
            self.set_title(HomeDirectory.shorten(file_name))
            self._raise("YUKI.N > set recent path", file_name)

    def _on_initialize(self):
        NagatoPortalLayer(self)
