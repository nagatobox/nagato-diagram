
from libnagato.ui.EventBox import NagatoEventBox as TFEI
from libnagatodiagram.ui.Grid import NagatoGrid
from libnagatodiagram.menu.context.ForChrome import NagatoContextMenu


class NagatoEventBox(TFEI):

    def _on_initialize(self):
        NagatoContextMenu(self)
        self._grid = NagatoGrid(self)
        self._raise("YUKI.N > add to main window", self)
