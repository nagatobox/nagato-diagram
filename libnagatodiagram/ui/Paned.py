
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagatotext.source.Page import NagatoPage
from libnagatoui.imageviewer.ImageViewer import NagatoImageViewer


class NagatoPaned(Gtk.Paned, NagatoObject):

    def _on_realize(self, widget):
        yuki_width = widget.get_allocated_width()
        self.set_position((yuki_width-16)/2)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_wide_handle(True)
        self.add2(NagatoImageViewer(self))
        self.add1(NagatoPage(self))
        self.connect("realize", self._on_realize)
        self._raise("YUKI.N > attach to grid", (self, (0, 0, 1, 1)))
