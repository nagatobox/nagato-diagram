
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libnagato.Object import NagatoObject

SUBPROCESS_FALGS = Gio.SubprocessFlags.NONE


class NagatoPixbufFactory(NagatoObject):

    def _create_new_pixbuf(self, image_path):
        try:
            yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(image_path)
            self._raise("YUKI.N > pixbuf created", yuki_pixbuf)
        except GLib.Error:
            self._raise("YUKI.N > data not found")
            self._raise("YUKI.N > null path queued")

    def _execute_dot(self, path):
        yuki_image_path = self._enquiry("YUKI.N > image path")
        yuki_args = ["dot", "-Tpng", path, "-o", yuki_image_path]
        yuki_subprocess = Gio.Subprocess.new(yuki_args, SUBPROCESS_FALGS)
        yuki_success = yuki_subprocess.wait(None)
        if yuki_success:
            self._create_new_pixbuf(yuki_image_path)

    def create_new_pixbuf(self, path):
        if path is None:
            self._raise("YUKI.N > null path queued")
        else:
            self._execute_dot(path)

    def __init__(self, parent):
        self._parent = parent
