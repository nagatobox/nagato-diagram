
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagato.Ux import Unit
from libnagato.menu.Separator import NagatoSeparator
# from libnagatotext.source.Page import NagatoPage
# from libnagatoui.imageviewer.ImageViewer import NagatoImageViewer
from libnagatodiagram.ui.Paned import NagatoPaned
from libnagatodiagram.menu.action.Export import NagatoExport


class NagatoGrid(Gtk.Grid, NagatoObject):

    def _yuki_n_additional_menus_for_drawing_area(self, parent):
        NagatoExport(parent)
        NagatoSeparator(parent)

    def _yuki_n_attach_to_grid(self, user_data):
        yuki_widget, yuki_geometries = user_data
        self.attach(yuki_widget, *yuki_geometries)

    def _initialize_grid(self):
        Gtk.Grid.__init__(self)
        self.set_border_width(Unit("grid-spacing"))
        self.set_row_spacing(Unit("grid-spacing"))
        self.set_column_spacing(Unit("grid-spacing"))
        self.set_column_homogeneous(True)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_grid()
        self._parent.add(self)
        NagatoPaned(self)
        # self.attach(NagatoImageViewer(self), 1, 0, 1, 1)
        # self.attach(NagatoPage(self), 0, 0, 1, 1)
