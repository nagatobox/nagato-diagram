
from libnagato.Object import NagatoObject
from libnagatodiagram.dataovermind.TempPathLayer import NagatoTempPathLayer
from libnagatoui.dataovermind.ImageViewerLayer import NagatoImageViewerLayer


class NagatoPortalLayer(NagatoObject):

    def _yuki_n_loopback_connection(self, parent_object):
        NagatoTempPathLayer(parent_object)

    def __init__(self, parent):
        self._parent = parent
        NagatoImageViewerLayer(self)
