
from gi.repository import Gio
from libnagato.Object import NagatoObject
from libnagatodiagram.dataovermind.DotLayer import NagatoDotLayer


class NagatoTempPathLayer(NagatoObject):

    def _inform_image_path(self):
        return self._image_path

    def __init__(self, parent):
        self._parent = parent
        yuki_temporary_file, _ = Gio.File.new_tmp(None)
        self._image_path = yuki_temporary_file.get_path()
        NagatoDotLayer(self)
