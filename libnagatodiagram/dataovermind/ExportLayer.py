
from libnagato.Object import NagatoObject
from libnagatodiagram.eventbox.ForGrid import NagatoEventBox
from libnagatodiagram.ui.PixbufExport import NagatoPixbufExport


class NagatoExportLayer(NagatoObject):

    def _yuki_n_export_graph(self):
        self._export.export()

    def __init__(self, parent):
        self._parent = parent
        self._export = NagatoPixbufExport(self)
        NagatoEventBox(self)
